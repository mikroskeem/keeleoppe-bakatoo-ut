# Keelemäng bakalaureusetöö TÜ

Siinne repositoorium on keeleõppe veebirakenduse loomise bakalaureusetöö jaoks. Töö autor on Tartu Ülikooli informaatika tudeng Janna-Liina Leemets. 

Keelemängus on kaks testitüüpi: grammatika- ja sõnavaratestid. Grammatikatestid sisaldavad endas käändeharjutusi, mis on koostatud Anneliis Hallingu poolt tema bakalaureusetöö jaoks. Sõnavaratestid koosnevad sünonüümiülesannetest, mille loomiseks on eraldi algoritm backend/src/scripts/meaningTest.py failis. Sünonüümitestide genereerimine toimub enne serveri töölepanekut, sest võtab mitmeid tunde. 

Selleks, et kohalikus arvutis ees- ja tagarakendust jooksutada, peab toimima järgnevalt:

**Tagarakenduse jaoks tuleb**
*  Paigaldada Anaconda, et conda keskkonda terminalis kasutada (võib tavaterminalis kui ka anaconda promptis, tavaterminali puhul peab conda lisama pathi). Pythoni ja EstNLTK jooksutamiseks on selline keskkond hädavajalik ja teistes virtualenvides ei tööta see nii hästi.
*  Installeerida python versiooni 3.5
*  Luua conda keskkonna pythoni versiooni 3.5-ga (`conda create --name nimi python=3.5 `)
*  Aktiveerida see keskkond, et järgmised teegid sinna installeerida (`conda activate nimi`)
*  Installeerida sqlalchemy (`conda install -c anaconda sqlalchemy`)
*  Installeerida psycopg2 (`conda install -c anaconda psycopg2`)
*  Installeerida flask (`conda install -c anaconda flask`)
*  Installeerida marshmallow (`conda install -c conda-forge marshmallow`)
*  Installeerida flask-cors (`conda install -c anaconda flask-cors`)
*  Installeerida jose-cryptodome (`conda install -c conda-forge python-jose-cryptodome`) PS! võib vajada eraldi jose paigaldamist conda käsuga
*  Installeerida EstNLTK 1.4 (`conda install -c valgur estnltk`)
*  Installeerida Gensim (`conda install -c anaconda gensim`)
*  Paigaldada arvutisse PostgreSQL (https://www.postgresql.org/download/)
*  Alla laadida repo failid ja teha kindlaks, et pordid 5000, 5432 ja 4200 oleksid vabad
*  Lisada faili entity.py kaustas backend/src/entities andmebaasi õige nimi, kasutaja ja parool
*  Esimest korda serverit käivitades main.py failis, mis asub kaustas backend/src, võtta kommentaarid ära faili alguses oleva koodibloki ümbert, mis loob andmebaasi tabelid ja lisab sinna testid
*  Käivitage Flaski server faili kaudu main.py olles terminaliga kaustas backend. Windows süsteemis käib see nii `python main.py`, Linux süsteemides `python3 main.py`

**Eesrakenduse jaoks**
*  Esimese sammuna on vaja Node-i ja NPM-i pakkide installeerimiseks ([installer](https://nodejs.org/en/download/))
*  Siis NPM-i kasutades terminalis installeerida Angular (`npm install -g @angular/cli`)
*  NPM-ga paigaldada Ng-Zorro (`npm i ng-zorro-antd`)
*  Paigaldada Auth0 (`npm i auth0-web@1.7.0`)
*  Käivitada eesrakendus eraldi terminalis käsuga `ng serve`, rakendus on leitav aadressil localhost:4200. 

Kui ükski `conda install` käskudest ei tööta, siis guugeldades "conda install x" leiab tihtipeale mitu käsku, millega proovida. Üks töötab ikka, kui teised mitte. Sünonüümitestide loomise algoritmi käivitamiseks üksinda piisab ka, kui installida ainult EstNLTK ja Gensim teegid. Sellisel juhul peab kutsuma seda funktsooni eraldi välja. Testide valimiseks on vaja juba kasutaja infot andmebassist, seega on vaja kõik installeerida ja käivitada.

Rakendus on saadaval ajutisel serveril aadressil http://167.71.3.184/.
 

