import { Component, OnInit, OnDestroy } from '@angular/core';
import * as Auth0 from "auth0-web";
import {getProfile} from "auth0-web";
import {UserTestService} from "../models/usertest/usertest.service";
import {Subscription} from "rxjs";
import {NzMessageService} from "ng-zorro-antd";

@Component({
  selector: 'app-profile',
  providers: [UserTestService],
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  subscriptions: Subscription = new Subscription();
  authenticated: boolean = false;
  profile: any;
  showDefaultImage: boolean = false;
  overallPercent: number = 0;
  grammarPercent: number = 0;
  meaningPercent: number = 0;
  wrongWords: string[] = [];
  wrongWordsGrammar: string[] = [];
  oaprogressFormat = () => this.overallPercent+`% vastustest valed`;
  gprogressFormat = () => this.grammarPercent+`% grammatikatestide vastustest valed`;
  mprogressFormat = () => this.meaningPercent+`% sünonüümitestide vastustest valed`;
  public filtersVisible1: boolean;
  public filtersVisible2: boolean;
  public filtersVisible3: boolean;
  iconType1: string = "down";
  iconType2: string = "down";
  iconType3: string = "down";

  constructor(private usertestService: UserTestService,  private message: NzMessageService) { }

  ngOnInit() {
    const self = this;
    Auth0.subscribe((authenticated) => (self.authenticated = authenticated));
    if (this.authenticated){
      this.profile = getProfile();
      this.subscriptions.add(this.usertestService.getData(this.profile['sub']).subscribe(resp => {
        this.overallPercent = resp[1];
        this.grammarPercent = resp[2];
        this.meaningPercent = resp[3];
        this.wrongWords = resp[0];
        this.wrongWordsGrammar = resp[4];
      }, error => this.message.create('error', 'Profiili kuvamisel läks midagi valesti. Logi välja ja uuesti sisse.')));
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  setDefault() {
    this.showDefaultImage=true;
  }

  collapseWindow(nr: number): void{
    if (nr == 1){
      if (this.filtersVisible1){
        this.filtersVisible1 = false;
        this.iconType1 = "down";
      }
      else {
        this.filtersVisible1=true;
        this.iconType1 = "up";
      }
    }
    if (nr == 2){
      if (this.filtersVisible2){
        this.filtersVisible2 = false;
        this.iconType2 = "down";
      }
      else {
        this.filtersVisible2=true;
        this.iconType2 = "up";
      }
    }
    else if (nr == 3) {
      if (this.filtersVisible3){
        this.filtersVisible3 = false;
        this.iconType3 = "down";
      }
      else {
        this.filtersVisible3=true;
        this.iconType3 = "up";
      }
    }
  }

}
