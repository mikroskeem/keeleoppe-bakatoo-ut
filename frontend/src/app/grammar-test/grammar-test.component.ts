import { Component, OnInit, OnDestroy } from '@angular/core';
import * as Auth0 from "auth0-web";
import {ActivatedRoute} from "@angular/router";
import {TestService} from "../models/tests/test.service";
import {Subscription} from "rxjs";
import {getProfile} from "auth0-web";
import {Test} from "../models/tests/test";
import {UserTest} from "../models/usertest/usertest";
import {UserTestService} from "../models/usertest/usertest.service";
import {NzMessageService} from "ng-zorro-antd";

@Component({
  selector: 'app-grammar-test',
  providers: [TestService, UserTestService],
  templateUrl: './grammar-test.component.html',
  styleUrls: ['./grammar-test.component.css']
})
export class GrammarTestComponent implements OnInit {
  private subscriptions: Subscription = new Subscription();
  authenticated: boolean = false;
  tests: Test[] = [];
  answeredTests: UserTest[] = [];
  showResults: boolean = false;
  sentenceCount: any = 5;
  score: number = 0;
  testNr: number = 0;

  constructor(private activatedroute: ActivatedRoute, private testService: TestService, private userTestService: UserTestService, private message: NzMessageService) { }

  ngOnInit() {
    this.sentenceCount=this.activatedroute.snapshot.paramMap.get("sentenceCount");
    const self = this;
    Auth0.subscribe((authenticated) => (self.authenticated = authenticated));
    if (this.authenticated) {
      this.subscriptions.add(this.testService.getTests(this.sentenceCount, getProfile()['sub'], 'g').subscribe(resp => {
        this.tests = resp;
      }, error => this.message.create('error', 'Testide pärimisel läks midagi valesti. Mine tagasi pealehele vajutades "Keelemäng" nimele!')));
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  saveResult(answer: string) {
    let correct = false;
    answer = answer.charAt(0).toLowerCase() + answer.slice(1);
    if (this.answeredTests.length != this.sentenceCount) {
      if (answer === this.tests[this.testNr][4]) {
        this.score = this.score + 1;
        correct = true;
      }
      let usertest = new UserTest(answer, correct, getProfile()['sub'], this.tests[this.testNr][5]);
      this.answeredTests.push(usertest);
      if (this.answeredTests.length != this.sentenceCount) this.testNr=this.testNr+1;
    }
    if (this.answeredTests.length == this.sentenceCount) {
      this.subscriptions.add(this.userTestService.submitTests(this.answeredTests, 'g').subscribe(resp => {
        }, error => console.log("Viga testide ärasaatmisel")
      ));
      this.showResults = true;
    }
  }

  deleteTest($event: boolean) {
    if ($event && this.tests.length != 1) {
      this.sentenceCount = this.sentenceCount - 1;
      this.tests.splice(this.testNr, 1);
    }
    else if ($event && this.tests.length == 1){
      window.location.reload();
    }
  }
}
