import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrammarTestComponent } from './grammar-test.component';

describe('GrammarTestComponent', () => {
  let component: GrammarTestComponent;
  let fixture: ComponentFixture<GrammarTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrammarTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrammarTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
