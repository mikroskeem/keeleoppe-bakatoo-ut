import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserService } from './models/users/user.service';
import {CallbackComponent} from "./callback.component";
import * as Auth0 from 'auth0-web';
import { NgZorroAntdModule, NZ_I18N, et_EE } from 'ng-zorro-antd';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import et from '@angular/common/locales/et';
import { MainComponent } from './main/main.component';
import { MeaningTestComponent } from './meaning-test/meaning-test.component';
import { TestPageComponent } from './test-page/test-page.component';
import { UnderlinePipe } from './pipes/underline.pipe';
import { ProfileComponent } from './profile/profile.component';
import { GrammarTestComponent } from './grammar-test/grammar-test.component';
import { StatusBarComponent } from './status-bar/status-bar.component';
import { ResultPageComponent } from './result-page/result-page.component';

registerLocaleData(et);

@NgModule({
  declarations: [
    AppComponent,
    CallbackComponent,
    MainComponent,
    MeaningTestComponent,
    TestPageComponent,
    UnderlinePipe,
    ProfileComponent,
    GrammarTestComponent,
    StatusBarComponent,
    ResultPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgZorroAntdModule,
    BrowserAnimationsModule
  ],
  providers: [UserService, { provide: NZ_I18N, useValue: et_EE }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    Auth0.configure({
      domain: 'dev-janna-ut.eu.auth0.com',
      audience: 'https://keelemang.bakatoo.ee',
      clientID: 'c1BsonKCJv1xxRnHJJoAUMcLIM7cGXtf',
      //redirectUri: 'http://localhost:4200/callback',
      redirectUri: 'http://167.71.3.184/callback',
      scope: 'openid profile manage:users'
    });
  }
}
