import { Pipe, PipeTransform } from '@angular/core';
import {split} from "ts-node";

@Pipe({
  name: 'underline'
})
export class UnderlinePipe implements PipeTransform {

  transform(sentence: string, word: string): any {
    let splitted = sentence.split(new RegExp(word+"+?\\b", 'ig'));
    if (splitted[0].length == 0){
      return '<u>'+this.capitalizeFirstLetter(word)+'</u>'+splitted[1];
    }
    return splitted[0]+'<u>'+word+'</u>'+splitted[1];
  }

  capitalizeFirstLetter(string: string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

}
