import {HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Injectable} from "@angular/core";
import {API_URL} from '../../env';
import * as Auth0 from "auth0-web";
import {UserTest} from "./usertest";

@Injectable()
export class UserTestService {

  constructor(private http: HttpClient) {
  }

  private extractData(res: Response) {
    return res || {};
  }

  public submitTests(usertests: UserTest[], test_type: string): Observable<any> {
    let params = new HttpParams().set('test_type', test_type);
    let headers = new HttpHeaders({
      'Authorization': `Bearer ${Auth0.getAccessToken()}`
    });
    return this.http.post(`${API_URL}/submit`, usertests, {params: params, responseType:'text', observe: 'response',  headers: headers});
  }

  public getData(user_id: string) {
    let params = new HttpParams().set('user_id', user_id);
    let headers = new HttpHeaders({
      'Authorization': `Bearer ${Auth0.getAccessToken()}`
    });
    return this.http.get(`${API_URL}/userdata`, {params: params, headers: headers}).pipe(map(this.extractData));
  }
}
