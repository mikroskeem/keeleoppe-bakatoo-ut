export class UserTest{
  constructor(
    public given_answer: string,
    public correct: boolean,
    public user_id: string,
    public question_id: number,
  ) { }
}
