export class Test{
    constructor(
        public createdAt: Date,
        public description: string,
        public _id: number,
        public testType: string,
        public updatedAt: Date,
    ) { }
}
