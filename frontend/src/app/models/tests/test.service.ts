import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Injectable} from "@angular/core";
import {API_URL} from '../../env';
import {Test} from './test';
import * as Auth0 from "auth0-web";

@Injectable()
export class TestService {


  constructor(private http: HttpClient) {
  }

  private extractData(res: Response) {
    return res || {};
  }

  public getAllTests(): Observable<Test[]> {
    return this.http.get(`${API_URL}/tests`).pipe(map(resp => resp as Test[]));
  }

  public getTests(sentenceCount: number, userId: string, type: string): Observable<any>{
    let params = new HttpParams().set('user_id', userId);
    return this.http.get(`${API_URL}/tests/`+type+`/`+sentenceCount, { params: params, headers: new HttpHeaders({
        'Authorization': `Bearer ${Auth0.getAccessToken()}`
      })
    }).pipe(map(this.extractData));
  }

  public reportTest(testId: number, testType: string): Observable<any> {
    let jsonObj = JSON.parse('{"test_id": "'+testId+'", "test_type": "'+testType+'"}');
    return this.http.post(`${API_URL}/tests/report`, jsonObj, {responseType:'text', observe: 'response', headers: new HttpHeaders({
        'Authorization': `Bearer ${Auth0.getAccessToken()}`
      })});
  }
}
