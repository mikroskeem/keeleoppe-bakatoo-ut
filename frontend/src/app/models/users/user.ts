export class User {
    constructor(
        public createdAt: Date,
        public user_id: string,
        public id: number,
        public name: string,
    ) { }
  }
