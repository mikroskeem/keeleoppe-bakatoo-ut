import {HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Injectable} from "@angular/core";
import {API_URL} from '../../env';
import {User} from './user';
import * as Auth0 from 'auth0-web';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  public saveUser(user: User): Observable<any> {
    return this.http.post(`${API_URL}/users`, user, {responseType:'text', observe: 'response', headers: new HttpHeaders({
        'Authorization': `Bearer ${Auth0.getAccessToken()}`
      })});
  }
}
