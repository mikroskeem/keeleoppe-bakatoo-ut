import * as Auth0 from 'auth0-web';
import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {getProfile} from "auth0-web";
import {UserService} from "./models/users/user.service";
import {Subscription} from "rxjs";
import {User} from "./models/users/user";
import {NzMessageService} from "ng-zorro-antd";

@Component({
  selector: 'callback',
  providers: [UserService],
  template: `
      <div class="outer">
        <div class="container"> 
          <h2>Laen autentimisandmeid...</h2>
        </div>
      </div>
  `,
  styles: ['.outer{\n' +
    '  text-align: center;\n' +
    '  box-sizing: border-box;\n' +
    '  min-height: 93vh;\n' +
    '}\n' +
    '.container{\n' +
    '  position: relative;\n' +
    '  top: 70px;\n' +
    '  text-align: center;\n' +
    '}']
})
export class CallbackComponent implements OnInit {
  private subscriptions: Subscription = new Subscription();

  constructor(private router: Router, private userService: UserService, private message: NzMessageService) { }

  authError(){
    this.message.create('error', 'Autentimisel esines viga. Proovi uuesti sisse logida.');
    setTimeout(() => {
      Auth0.signOut();
      this.router.navigate(['/']);
    }, 4000);
  }

  ngOnInit(): void {
    const self = this;
    Auth0.handleAuthCallback((err) => {
      if (err) this.authError();
      this.subscriptions.add(this.userService.saveUser(new User(null, getProfile()['sub'], null, getProfile()['name'])).subscribe(
        resp => {
          if (resp.status == 200 || resp.status == 201) this.router.navigate(['/']);
          else {
            this.message.create('warning', 'Autentimisel esines viga. Proovi uuesti sisse logida.');
            setTimeout(() => {
              Auth0.signOut();
              this.router.navigate(['/']);
              }, 4000);
          }
        },
        error => this.authError()
        ));
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
