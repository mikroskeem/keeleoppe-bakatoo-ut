import { Component, OnInit } from '@angular/core';
import * as Auth0 from 'auth0-web';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  authenticated = false;
  sentenceCount: number = 5;
  signIn = Auth0.signIn;
  getProfile = Auth0.getProfile;

  constructor() { }

  ngOnInit() {
    const self = this;
    Auth0.subscribe((authenticated) => (self.authenticated = authenticated));
  }
  login (){
    Auth0.isAuthenticated()
  }
}
