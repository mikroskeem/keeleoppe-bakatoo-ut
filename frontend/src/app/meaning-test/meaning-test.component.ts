import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import * as Auth0 from "auth0-web";
import {TestService} from "../models/tests/test.service";
import {Subscription} from "rxjs";
import {getProfile} from "auth0-web";
import {UserTest} from "../models/usertest/usertest";
import {UserTestService} from "../models/usertest/usertest.service";
import {ActivatedRoute} from "@angular/router";
import {NzMessageService} from "ng-zorro-antd";

@Component({
  selector: 'app-meaning-test',
  templateUrl: './meaning-test.component.html',
  providers: [TestService, UserTestService],
  styleUrls: ['./meaning-test.component.css']
})
export class MeaningTestComponent implements OnInit {
  private subscriptions: Subscription = new Subscription();
  authenticated = false;
  showResults: boolean;
  sentenceCount: any = 5;
  testNr: number = 0;
  tests: any[] = [];
  score: number = 0;
  answeredTests: UserTest[] = [];

  constructor(private testService: TestService, private userTestService: UserTestService, private activatedroute:ActivatedRoute, private message: NzMessageService) {}

  ngOnInit() {
    this.sentenceCount=this.activatedroute.snapshot.paramMap.get("sentenceCount");
    const self = this;
    Auth0.subscribe((authenticated) => (self.authenticated = authenticated));
    if (this.authenticated) {
      this.subscriptions.add(this.testService.getTests(this.sentenceCount, getProfile()['sub'], 'm').subscribe( resp => {
          this.tests = resp;
        }, error => this.message.create('error', 'Testide pärimisel läks midagi valesti. Mine tagasi pealehele vajutades "Keelemäng" nimele!')
      ));
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  saveResult(answer: string) {
    let correct = false;
    if (this.answeredTests.length != this.sentenceCount) {
      if (answer === this.tests[this.testNr][4]) {
        this.score = this.score = this.score + 1;
        correct = true;
      }
      let usertest = new UserTest(answer, correct, getProfile()['sub'], this.tests[this.testNr][5]);
      this.answeredTests.push(usertest);
      if (this.answeredTests.length != this.sentenceCount) this.testNr=this.testNr+1;
    }
    if (this.answeredTests.length == this.sentenceCount) {
      this.subscriptions.add(this.userTestService.submitTests(this.answeredTests, 'm').subscribe(resp => {
      }, error => console.log("Viga testide ärasaatmisel!")
      ));
      this.showResults = true;
    }
  }

  deleteTest($event: boolean) {
    if ($event && this.tests.length != 1) {
      this.sentenceCount = this.sentenceCount - 1;
      this.tests.splice(this.testNr, 1);
    }
    else if ($event && this.tests.length == 1){
      window.location.reload();
    }
  }
}
