import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeaningTestComponent } from './meaning-test.component';

describe('MeaningTestComponent', () => {
  let component: MeaningTestComponent;
  let fixture: ComponentFixture<MeaningTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeaningTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeaningTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
