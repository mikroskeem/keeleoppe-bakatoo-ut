import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import * as Auth0 from "auth0-web";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Keelemäng';
  private subscription: Subscription = new Subscription();
  authenticated = false;

  signOut = Auth0.signOut;

  constructor() {}

  ngOnInit() {
    const self = this;
    Auth0.subscribe((authenticated) => (self.authenticated = authenticated));
  }

  ngOnDestroy(): void{
    this.subscription.unsubscribe();
  }
}
