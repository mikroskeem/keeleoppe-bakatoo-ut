import {Component, EventEmitter, Input, OnInit, Output, OnDestroy} from '@angular/core';
import {UnderlinePipe} from "../pipes/underline.pipe";
import {TestService} from "../models/tests/test.service";
import {NzMessageService} from "ng-zorro-antd";
import {Subscription} from "rxjs";
import {Router} from '@angular/router';

@Component({
  selector: 'app-test-page',
  providers: [UnderlinePipe, TestService],
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.css']
})
export class TestPageComponent implements OnInit {
  private subscriptions: Subscription = new Subscription();

  constructor(private pipe: UnderlinePipe, private testService: TestService, private message: NzMessageService,  private router: Router) { }

  ngOnInit() {}

  @Input('testType') type: string;
  @Input('testQuestion') question: string;
  @Input('testWordFormatted') formattedWord: string;
  @Input('testWord') word: string;
  @Input('testID') testId: number;
  @Input('testAnswer') answer: string;
  @Input('testOptions') options: string[];
  @Output() answeredCorrectly = new EventEmitter<string>();
  @Output() reportedTest = new EventEmitter<boolean>();
  answerChoice: string;
  isVisible: boolean = false;

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  submit() {
    this.answeredCorrectly.emit(this.answerChoice);
    this.answerChoice = null;
  }

  report() {
    this.isVisible = true;
  }
  close() {
    this.isVisible = false;
  }
  handleOk() {
    this.subscriptions.add(this.testService.reportTest(this.testId, this.type).subscribe(resp => {
      if (resp.status == 200) {
        this.message.create('success', 'Sinu sõnum edastati edukalt');
      }
      else if (resp.status != 200) {
        this.message.create('error', 'Sinu sõnumi edastamisel läks midagi valesti');
      }
    }, error => this.message.create('error', 'Sinu sõnumi edastamisel läks midagi valesti')));
    this.isVisible = false;
    this.reportedTest.emit(true);
  }
}
