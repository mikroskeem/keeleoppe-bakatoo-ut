import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.css']
})
export class StatusBarComponent implements OnInit {

  @Input('score') score: number = 0;
  @Input('sentenceCount') sentNo: number = 0;
  @Input('testNr') nr: number = 0;
  progressFormat = () => this.nr+`/`+this.sentNo;

  constructor() { }

  ngOnInit() {
  }

}
