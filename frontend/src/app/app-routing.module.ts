import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CallbackComponent} from "./callback.component";
import {MainComponent} from "./main/main.component";
import {MeaningTestComponent} from "./meaning-test/meaning-test.component";
import {ProfileComponent} from "./profile/profile.component";
import {GrammarTestComponent} from "./grammar-test/grammar-test.component";

const routes: Routes = [
  { path: '', component: MainComponent},
  { path: 'callback', component: CallbackComponent},
  { path: 'meaning/:sentenceCount', component: MeaningTestComponent},
  { path: 'grammar/:sentenceCount', component: GrammarTestComponent},
  { path: 'profile', component: ProfileComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
