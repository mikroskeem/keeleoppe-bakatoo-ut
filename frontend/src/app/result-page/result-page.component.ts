import {Component, Input, OnInit} from '@angular/core';
import {UserTest} from "../models/usertest/usertest";

@Component({
  selector: 'app-result-page',
  templateUrl: './result-page.component.html',
  styleUrls: ['./result-page.component.css']
})
export class ResultPageComponent implements OnInit {

  @Input('score') score: number = 0;
  @Input('ttype') ttype: string = "";
  @Input('sentenceCount') sentenceCount: number = 0;
  @Input('tests') tests = [];
  @Input ('answers') answers: UserTest[] = [];

  constructor() { }

  ngOnInit() {
  }

  refresh() {
    window.location.reload();
  }
}
