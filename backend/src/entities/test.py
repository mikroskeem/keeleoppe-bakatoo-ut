# coding=utf-8

from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.associationproxy import association_proxy
from marshmallow import Schema, fields, post_load
from sqlalchemy.orm import relationship
from .entity import Entity, Base
from .user2test import UserTest

class Test(Entity, Base):
    """ Testide esindusklass.

    Atribuudid:
    question_id -- testi unikaalne ID, failist saadud
    correct_answer -- õige vastus küsimusele
    test_type -- kas tegu sünonüümi- või käändetestiga
    users -- one-to-many seos user2test klassiga
    """
    __tablename__ = 'tests'
    question_id = Column(Integer, primary_key=True)
    correct_answer = Column(String(30), nullable=False)
    test_type = Column(String(10), nullable=False)
    users = association_proxy('user_association', 'user', creator=lambda u: UserTest(user=u))

    def __init__(self, question_id, correct_answer, test_type):
        Entity.__init__(self)
        self.question_id = question_id
        self.correct_answer = correct_answer
        self.test_type = test_type
        
    def __repr__(self):
        return "<Question='%s', correct answer='%s', type='%s')>" % (
                             self.question_id, self.correct_answer, self.test_type)
                             
class TestSchema(Schema):
    """ Testi mall, millega saab JSON-objektist luua Test objekti """
    id = fields.Number()
    question_id = fields.Number()
    correct_answer = fields.Str()
    test_type = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()

    @post_load
    def make_test(self, data, **kwargs):
        return Test(**data)