# coding=utf-8

from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.ext.associationproxy import association_proxy
from marshmallow import Schema, fields, post_load
from .entity import Entity, Base
from .test import Test, TestSchema
from .user2test import UserTest

class User(Entity, Base):
    """ Kasutajate klass, millega ühendatakse Keelemäng ja Auth0 kasutajad

    Atribuudid:
    user_id -- Auth0-lt saadud kasutaja identifikaator, mille abil vaadatakse, mis kasutajaga on tegu
    name -- kasutaja nimi, samuti Auth0-lt saadud
    usergrades -- one to many seos hinnetega, ühel kasutajal on palju hindeid
    tests -- one to many seos tehtud testidega, et statistikat koguda kasutaja toimetuleku kohta
    """
    __tablename__ = 'users'
    user_id = Column(String(200), primary_key=True)
    name = Column(String(30))
    usergrades = relationship('Grades', back_populates="users")
    tests = association_proxy('test_association', 'test', creator=lambda t:UserTest(test=t))

    def __init__(self, user_id, name):
        Entity.__init__(self)
        self.user_id = user_id
        self.name = name

    def __repr__(self):
        return "<User(name='%s', id='%s')>" % (
                             self.name, self.user_id)
class UserSchema(Schema):
    """ Kasutaja mall, millega saab JSON-objektist luua User objekti """
    id = fields.Number()
    name = fields.Str()
    user_id = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
    tests = fields.Nested(TestSchema(only=("question_id",)))

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)

