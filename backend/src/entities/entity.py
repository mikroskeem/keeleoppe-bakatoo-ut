# coding=utf-8
"""Olemite kirjeldamise fail. Selles failis ühendutakse ka andmebaasiga."""
import os
from datetime import datetime
from sqlalchemy import create_engine, Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

db_url = 'localhost:5432' #AB port
db_name = 'km' #AB nimi
db_user = 'postgres' # Siia AB kasutajanimi
db_password = 'postgres' # Siia AB parool
db_conn_url = 'postgresql://'+db_user+':'+db_password+'@'+db_url+'/'+db_name
engine = create_engine(os.getenv('DATABASE_URL', db_conn_url))
Session = sessionmaker(bind=engine)

Base = declarative_base()

class Entity():
    """Olemite alusklass. See klass on põhjaks kõikidele olemiklassidele, mida hoitakse andmebaasis.

    Atribuudid:
    created_at -- objekti loomisaeg
    updated_at -- viimane aeg, millal objekti uuendati
    """
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


    def __init__(self):
        """Initsialiseerib klassid loomis- ja muutmisajaga"""
        self.created_at = datetime.now()
        self.updated_at = datetime.now()
