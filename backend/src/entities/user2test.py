# coding=utf-8

from sqlalchemy import Column, String, Integer, Boolean, DateTime, ForeignKey, Table
from sqlalchemy.orm import relationship
from marshmallow import Schema, fields
from datetime import datetime
from .entity import Entity, Base
#from .user import UserSchema
#from .test import TestSchema


'''usertest = Table('usertest',
                 Column('user_id', Integer, ForeignKey('user.user_id')),
                 Column('test_id', Integer, ForeignKey('test.test_id')),
                 Column('correct', Boolean),
                 Column('given_answer', String(30)))'''

class UserTest(Entity, Base):
    """ Kasutaja tehtud testide Association Object ehk many-to-many seose vaheklass User ja Test klassidele

    Atribuudid:
    id -- unikaalne ID
    given_answer -- mis vastuse kasutaja testis tegelikult andis
    correct -- kas vastus oli õige või vale
    user_id -- välisvõti tabelile users
    question_id -- välisvõti tabelile tests
    """
    __tablename__ = 'usertest'
    id = Column(Integer, primary_key=True)
    given_answer = Column(String(30), nullable=False)
    correct = Column(Boolean, nullable=False)
    user_id = Column('user_id', String, ForeignKey('users.user_id'))
    question_id = Column('question_id', Integer, ForeignKey('tests.question_id'))

    test = relationship("Test", backref='user_association')
    user = relationship("User", backref='test_association')

    def __init__(self, given_answer, correct, user_id, test_id):
        Entity.__init__(self)
        self.given_answer = given_answer
        self.correct = correct
        self.user_id = user_id
        self.question_id = test_id

    def __repr__(self):
        return "<Usertest(name='%s')>" % (
            self.given_answer)
