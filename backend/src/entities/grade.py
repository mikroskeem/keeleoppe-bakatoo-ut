# coding=utf-8

from sqlalchemy import Column, String, Integer, Boolean, DateTime, ForeignKey, Table, Enum, ARRAY
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy.orm import relationship
from .entity import Entity, Base
import enum

class GradeCategories(enum.Enum):
    """Hinnete enum-klass. Hinne 1 on halvim, hinne 3 parim"""
    one = 1
    two = 2
    three = 3

class Grades(Entity, Base):
    """Hinnete klass, kus iga sõna on oma unikaalne objekt. Käändetestide puhul on unikaalne sõna-käände kombinatsioon

    Atribuudid:
    grade -- hinne, mis on enum-klassi tüüpi
    question -- sõna, mille hindega on tegu
    user_id -- kellele see hinne kuulub
    done_question_ids -- testid, millele kasutaja on vastanud ja mis sisaldavad seda küsitavat sõna ehk questionit
    word_case -- kääne, mittekohustuslik veerg. Lisatakse ainult käändetestide puhul
    users -- one to many suhe tabeliga users
    """
    __tablename__ = 'grades'
    id = Column(Integer, primary_key=True)
    grade = Column(Enum(GradeCategories), nullable=False)
    question = Column(String(30), unique=True)
    user_id = Column('user_id', String, ForeignKey('users.user_id'))
    done_question_ids = Column(MutableList.as_mutable(ARRAY(Integer)))
    word_case= Column(String(30))
    users = relationship("User", back_populates="usergrades")

    def __init__(self, grade, question, user_id, test_id, case=''):
        Entity.__init__(self)
        self.grade = grade
        self.question = question
        self.user_id = user_id
        self.done_question_ids = [test_id]
        self.word_case = case

    def add_question_id(self, question_id):
        """ Meetod hindele testi ID lisamiseks """
        if question_id not in self.done_question_ids:
            self.done_question_ids.append(question_id)

    def delete_question_id(self, question_id):
        """ Meetod hindest testi ID eemaldamiseks """
        if question_id in self.done_question_ids:
            self.done_question_ids.remove(question_id)

    def __repr__(self):
        return "<Grade(grade='%s',word='%s')>" % (
            self.grade, self.question)
