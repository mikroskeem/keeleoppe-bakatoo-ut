# -*- coding: utf-8 -*-
"""Grammatikatestide sisselugemise ja valimise skript

Seda faili saab moodulina importida ning sisaldab järgmisi funktsioone:
    * replaceBack - Asendab %%% karakterid käänatud sõnaga
    * readInTests - loeb sisse koik_laused_utf8.xml failist kõik testid ja tagastab nende loendi
    * chooseTests - valib kasutajale kohanduvalt testid
"""
from xml.etree import ElementTree
import random

def readInTests(unsuitableIds):
    """Loeb testid sisse tekstina XML-failist ja eraldab teksti osadeks

    Parameetrid:
    unsuitableIds -- sobimatute lausete ID-d, mida ei tohi kaasa lugeda

    Tagastab:
    tests -- testide loend, kus on erinevad osad tükkideks võetud
    """
    tests = []
    with open('koik_laused_utf8.xml', 'r', encoding="utf-8") as f:
        e = ElementTree.parse(f)
        root = e.getroot()
        for i,child in enumerate(root):
            # Me soovime lihtsalt sisseütlevat käänet, võtame lühikesed sisseütlevad küsimused välja
            if str(int(child.attrib['id'])+828) in unsuitableIds or child.find('case').text == 'lühike sisseütlev':
                continue
            # 828 lisame id-le, sest andmebaasis on vaja unikaalseid küsimuse identifikaatoreid ja esimesed 828 kohta on võetud sünonüümitestide poolt
            test = [int(child.attrib['id'])+828, replaceBack(child.find('s').text, child.find('word').text), child.find('nr').text, child.find('case').text, child.find('n').text, child.find('word').text]
            tests.append(test)
    return tests

def replaceBack(sent, word):
    """Hallingu loodud testides on küsitavad sõnad eemaldatud ühe testitüübi jaoks. Sõna asemel on lünk kujul "%%%".
    Meie testides pole lünka vaja, sest küsime sõna käände kohta, mitte ei taha, et kasutaja käändes sõna sisestaks. Selleks peame sõna tagasi panema lausesse.

    Parameetrid:
    sent -- lause, kuhu sõna tagasi asendada
    word -- sõna, mida tagasi asendada

    Tagastab:
    lause -- lause koos sõnaga
    """
    return sent.replace('%%%', word)

def chooseTests(sentenceNr, tests, grades):
    """Valib kasutajale käändetestid olenevalt tema eelnevatest hinnetest
    Kui kasutajal pole ühtegi hinnet, valitakse testid suvaliselt.
    Kui kasutajal on hindeid, arvutatakse igale hindele jaotus ja valitakse suvaliselt testide hulgast nii palju ülesandeid, et jaotused
    täis saada.

    Parameetrid:
    sentenceNr -- number, mitu testi on vaja
    tests -- testide loend, millest valitakse
    grades -- kasutaja hinded

    Tagastab:
    chosenTests -- valitud testid
    """
    # Vastused, mille paneme lõpus kaasa testidele
    options =['Nimetav','Omastav','Osastav','Sisseütlev','Seesütlev','Seestütlev','Alaleütlev','Alalütlev','Alaltütlev','Saav','Rajav','Olev','Ilmaütlev','Kaasaütlev']
    chosenTests = []
    choices = []
    # Kui kasutajal pole hindeid, pole ta järelikult ühtegi testi veel teinud. Valime testid suvaliselt
    if len(grades) == 0:
        questions = [] # Jätame meelde juba valitud küsitavad sõnad eraldi, et ei peaks valikute loendit filtreerima pidevalt
        while len(choices) != sentenceNr:
            choice = random.choice(tests) # Valime suvalise testi
            # Lisame sõna ja käände kombinatsioone, mis on unikaalsed
            if choice not in choices and (choice[4], choice[3]) not in questions:
                choices.append(choice)
                questions.append((choice[4], choice[3]))
    # Kui hinded on olemas, peame hakkama hinnetepõhiselt valima
    else:
        # Jätame tulevikuks meelde ka kõik testide ID-d, mida kasutaja on juba näinud
        ids = [item for sublist in grades for item in sublist[2]]
        # Filtreerime hinnetest küsitavad sõnad koos käänetega eraldi listidesse, et leida selle käänatud sõnaga uusi ülesandeid leida
        ones = [(item[1], item[3]) for item in list(filter(lambda x: x[0] == 'one', grades))]
        twos = [(item[1], item[3]) for item in list(filter(lambda x: x[0] == 'two', grades))]
        threes = [(item[1], item[3]) for item in list(filter(lambda x: x[0] == 'three', grades))]
        # Arvutame välja jaotused, need sõltuvad küsimuste arvust
        oneCount = sentenceNr*60/100
        twoCount = sentenceNr*20/100
        threeCount = 0
        if sentenceNr == 15:
            threeCount = sentenceNr*10/100
        # Alustame kõige tähtsamatest sõnadest ehk sõnadest, mille skoor on 1
        idx = 0
        taken = []
        # Kõigepealt proovime käändega koos sõna lisada, selleks kontrollime, kas selliseid teste käände ja sõnaga on
        for item in ones:
            if len(choices) == oneCount: # Kontroll, kas on piisavalt juba lisatud esimesi
                break
            for it in tests: # Vaatame järjest teste läbi, otsime õigeid
                if it[4] == item[0] and it[3]==item[1] and it[0] not in ids: #Kui sõna on õige, kääne on õige ja testi pole juba nöidatud, lisame
                    choices.append(it)
                    idx += 1
                    taken.append(it[4])
                    break # Leidsime vastavaja sõna-käände kombinatsiooni, rohkem pole vaja seda spetsiifilist sõna-käänet otsida
        # Siia jõuame, kui sõna-käände kombinatsiooniga teste rohkem polnud. Nüüd otsime ainult sõna järgi
        while len(choices) < oneCount:
            choice = random.choice(tests)
            if any(choice[4] in i for i in ones) and choice[4] not in taken: # See test sobib, kui selle küsitav sõnal on hinne olemas ja seda pole valitud
                choices.append(choice)
                taken.append(choice[4])
                idx += 1
            if len(ones) == idx: # Kui lisasime testid kõikide selle hindega sõnadega ära, pole meil midagi rohkem lisada
                break # Liigume järgmise hindekategooria juurde
        # Järgmisena võtame tähed, mille skoor on 2, ehk keskmine. Kõik kordub nagu eelmise hinde puhul
        currentLen = len(choices)
        idx = 0 # Taastame indeksi 0-ks, sest tegeleme järgmise hindekategooriaga
        for item in twos:
            if len(choices) == int(currentLen + twoCount):
                break
            for it in tests:
                if it[4] == item[0] and it[3]==item[1] and it[0] not in ids:
                    choices.append(it)
                    idx += 1
                    taken.append(it[4])
                    break
        currentLen = len(choices)
        while len(choices) < currentLen + twoCount:
            choice = random.choice(tests)
            if any(choice[4] in i for i in twos) and choice[4] not in taken:
                choices.append(choice)
                taken.append(choice[4])
                idx += 1
            if len(twos) == idx:
                break
        # Viimaks vaatame kolmese skooriga sõnu. Kõik kordub, nagu eelmise hinde puhul.
        currentLen = len(choices)
        idx = 0
        for item in threes:
            if len(choices) == currentLen + threeCount:
                break
            for it in tests:
                if it[4] == item[0] and it[3]==item[1] and it[0] not in ids:
                    choices.append(it)
                    idx += 1
                    taken.append(it[4])
                    break
        currentLen = len(choices)
        while len(choices) < currentLen + threeCount:
            choice = random.choice(tests)
            if any(choice[4] in i for i in threes)  and choice[4] not in taken:
                choices.append(choice)
                taken.append(choice[4])
                idx += 1
            if len(threes) == idx:
                break
        # Kontrollime, kas on üldse kasutajale tundmatuid sõnu. Kui ei ole ja küsimusi on veel vaja, võtame suvaliselt juurde
        if len(tests) == len(ids):
            choices = random.sample(tests, len(choices)-sentenceNr)
        else:
            # Kui enne nägemata sõnu pole rohkem, siis valime lihtsalt suvalise küsimuse
            while len(choices) != sentenceNr:
                choice = random.choice(tests)
                if choice[4] not in taken:
                    choices.append(choice)
                    taken.append(choice[4])
    for choice in choices:
        # Vormistame tulemuse ümber eesrakenduse jaoks
        result = [choice[1], choice[5], choice[4], options, choice[3], choice[0]]
        chosenTests.append(result)
    return chosenTests