# -*- coding: utf-8 -*-
"""Sünonüümitestide loomise ja valimise skript

See skript loob korpuse põhjal sünonüümitestid, loeb neid failist sisse kiire juurdepääsu tagamiseks ning valib neid kasutajale välja

See skript nõuab, et EstNTLK oleksid keskkonda installeeritud.

Seda faili saab moodulina importida ning sisaldab järgmisi funktsioone:
    * untokenize - teeb EstNLTK Text objektist uuesti tavalise lause
    * readInTests - loeb sisse synonymTests.txt failist kõik testid ja tagastab nende loendi
    * chooseTests - valib kasutajale kohanduvalt testid
"""
import readcorp as corp
import random
import estnltk
from estnltk.wordnet import wn
import re

def untokenize(sentence):
    """Teeb Text objektist tavalause

    Parameetrid:
    sentence -- lause, mida muudetakse

    Tagastab:
    result -- normaliseeritud lause
    """
    text = estnltk.Text(sentence)
    tokens = text.word_texts
    result = ' '.join(tokens).replace(' , ',',').replace(' .','.').replace(' !','!')
    if "," in result:
        # Juhul, kui lauses on koma, peame teda eraldi käsitlema.
        # On 2 juhtu üldiselt: ta on kas numbrite vahel nagu "3,3 miljardit" või tavakasutuses
        # Et mõlemat käsitleda kasutame regexit
        result = re.sub(r'(?<=[,])((?![0-9])(?=[^\s]))', r' ', result)
    result = result.replace(' ?','?').replace(' : ',': ').replace(' \'', '\'')
    return result

def readInTests(unsuitableIds):
    """Loeb testid sisse tekstina .txt failist ja eraldab teksti osadeks

    Parameetrid:
    unsuitableIds -- sobimatute lausete ID-d, mida ei tohi kaasa lugeda

    Tagastab:
    tests -- testide loend, kus on erinevad osad tükkideks võetud
    """
    tests = []
    with open('synonymTests.txt', "r", encoding="utf-8") as f:
        sentences = f.read().splitlines()
        for sentence in sentences:
            result = [piece for piece in sentence.split("STP")]
            if result[0] in unsuitableIds:
                continue
            result[4] = result[4].split(",")
            if result[5] not in result[4]:
               result[4].append(result[5])
            tests.append(result)
    return tests

def chooseTests(sentenceNr, tests, grades):
    """Valib kasutajale sünonüümitestid olenevalt tema eelnevatest hinnetest
    Kui kasutajal pole ühtegi hinnet, valitakse testid suvaliselt.
    Kui kasutajal on hindeid, arvutatakse igale hindele jaotus ja valitakse suvaliselt testide hulgast nii palju ülesandeid, et jaotused
    täis saada.

    Parameetrid:
    sentenceNr -- number, mitu testi on vaja
    tests -- testide loend, millest valitakse
    grades -- kasutaja hinded

    Tagastab:
    chosenTests -- valitud testid
    """
    choices = [] # Valitud testid
    # Kui kasutajal pole hindeid, pole ta järelikult ühtegi testi veel teinud. Valime testid suvaliselt
    if len(grades) == 0:
        questions = [] # Jätame meelde juba valitud küsitavad sõnad eraldi, et ei peaks valikute loendit filtreerima pidevalt
        while len(choices) != sentenceNr:
            choice = random.choice(tests) # Valime suvalise testi
            # Kui see test ja selles olev küsitav sõna pole juba valitud, lisame
            if choice not in choices and choice[3] not in questions:
                choices.append(choice)
                questions.append(choice[3])
    # Kui hinded on olemas, peame hakkama hinnetepõhiselt valima
    else:
        # Jätame tulevikuks meelde kõik unikaalsed küsitavad sõnad
        words = set()
        for test in tests:
            words.add(test[3])
        # Jätame tulevikuks meelde ka kõik testide ID-d, mida kasutaja on juba näinud
        ids = [item for sublist in grades for item in sublist[2]]
        # Filtreerime hinnetest küsitavad sõnad eraldi listidesse, et nende küsitavate sõnadega uusi ülesandeid leida
        ones = [item[1] for item in list(filter(lambda x: x[0] == 'one', grades))] # Kõige halvem hinne
        twos = [item[1] for item in list(filter(lambda x: x[0] == 'two', grades))]
        threes = [item[1] for item in list(filter(lambda x: x[0] == 'three', grades))] # Kõige parem hinne
        # Arvutame välja jaotused, need sõltuvad küsimuste arvust
        oneCount = sentenceNr*60/100
        twoCount = sentenceNr*20/100
        threeCount = 0
        if sentenceNr == 15: # Hindega kolm ehk kõige kasutaja jaoks kõige paremini teatavaid sõnu tahame kordamise mõttes näha ainult 15 lause puhul.
            threeCount = sentenceNr*10/100

        # Alustame kordamises kõige tähtsamatest sõnadest ehk sõnadest, mille skoor on 1
        taken = [] # Juba valitud küsitud sõnade ja vastuste paarid kujul (sõna, vastus)
        idx = 0
        while len(choices) < oneCount: # Kuniks jaotus pole täis, kordame
            choice = random.choice(tests)
            if choice[3] in ones and (choice[3], choice[5]) not in taken: # Lisame, kui küsitaval sõnal on hindeks üks ja sellist sõna-vastuse kombinatsiooni pole veel valitud
                choices.append(choice)
                taken.append((choice[3], choice[5]))
                idx += 1
            if len(ones) == idx: # Kui lisasime testid kõikide selle hindega sõnadega ära, pole meil midagi rohkem lisada
                break # Liigume järgmise hindekategooria juurde
        # Järgmisena võtame tähed, mille skoor on 2, ehk keskmine. Kordame sama, mis eelmise tsükli puhul
        idx = 0 # Taastame indeksi 0-ks, sest tegeleme järgmise hindekategooriaga
        currentLen = len(choices)
        while len(choices) < currentLen + twoCount:
            choice = random.choice(tests)
            if choice[3] in twos and (choice[3], choice[5]) not in taken:
                choices.append(choice)
                taken.append((choice[3], choice[5]))
                idx += 1
            if len(twos) == idx:
                break
        # Viimaks vaatame kolmese skooriga sõnu. Kõik kordub selles tsüklis nii nagu eelmistes
        idx = 0
        currentLen = len(choices)
        while len(choices) < currentLen+threeCount:
            choice = random.choice(tests)
            if choice[3] in threes and (choice[3], choice[5]) not in taken:
                choices.append(choice)
                taken.append((choice[3], choice[5]))
                idx += 1
            if len(threes) == idx:
                break
        # Kontrollime, kas on üldse kasutajale tundmatuid sõnu. Kui ei ole ja küsimusi on veel vaja, võtame suvaliselt juurde
        if len(tests) == len(ids):
            choices.extend(random.sample(tests, sentenceNr-len(choices)))
        # Kui on tundmatuid sõnu, lisame neid
        else:
            uniqueCount = 0 # Uute enne nägemata testide arvu meeldejätmiseks
            while len(choices) != sentenceNr:
                choice = random.choice(tests)
                # Jätkame, kui on ikka veel tundmatuid sõnu olemas
                if len(ones) + len(twos) + len(threes) + uniqueCount < len(words):
                    # Kui sõna pole hindena kirjas ja pole veel lisatud koos tema vastusega, siis jätkame
                    if choice[3] not in ones and choice[3] not in twos and choice[3] not in threes and (choice[3], choice[5]) not in taken:
                        choices.append(choice)
                        taken.append((choice[3], choice[5]))
                        uniqueCount += 1
                # Kui enne nägemata sõnu pole rohkem, siis valime lihtsalt suvalise küsimuse
                elif (choice[3], choice[5]) not in taken:
                    choices.append(choice)
                    taken.append((choice[3], choice[5]))
    chosenTests = [] # Eraldi loend õigesti vormistatud küsimuste hoidmiseks
    for choice in choices:
        # Vormistame tulemuse ümber eesrakenduse jaoks
        random.shuffle(choice[4]) # Segame omavahel valikuvastused, et nad ei oleks alati samas järjekorras
        chosenTests.append([choice[1],choice[2],choice[3],choice[4],choice[5], choice[0]])
    return chosenTests

def createTests():
    """Sünonüümitestide loomise meetod
    Funktsioon valib korpusest sobivad sõnad, otsib neile sünonüümid ja sarnased sõnad.
    Funktsioon ei tagasta midagi, vaid kirjutab tulemuse faili "synonymTests.txt"
    """
    wn.all_synsets() # Wordneti töö kiirendamiseks
    conversion = {'S': wn.NOUN, 'V': wn.VERB, 'A': wn.ADJ, 'D': wn.ADV} # Wordneti ja EstNLTK sõnavormide ühendamiseks
    # Loome korpuse ja Word2Vec mudeli
    corpus, model = corp.createCorpus()
    # Hoiame eraldi meeles kõiki kuid erinevatel kujudel, et neid mitte kaasata
    months = ['jaanuar', 'aprill', 'mai', 'juuni', 'august', 'oktoober', 'oktoobrikuu', 'november', 'detsember', 'juuli', 'märts', 'september', 'esmaspäev', 'teisipäev', 'kolmapäev', 'neljapäev', 'reede', 'laupäev']

    writtenSentences = [] # Loend vältimaks sama lause ja küsitava sõnaga testide duplikaate
    writtenSynonymsPerWord = {}
    idx = 0 # Testidele antav unikaalne identifikaator
    with open('synonymTests.txt', "w", encoding="utf-8") as f:
        # Läbime korpuse lausehaaval
        for sentence in corpus:
            sent = estnltk.Text(sentence) # Teeme lausest Text objekti, et seda morfoloogiliselt analüüsida
            # Sõna algvorm, kääne ja käänatud vorm
            tagged = list(zip(sent.lemmas, sent.postags, sent.word_texts))
            # Läbime lause sõnahaaval
            for word in tagged:
                # Jätkame, kui sõna on nimisõna, omadussõna või verb, ei ole kuunimi ja seda lauset ja sõna testina veel pole
                if (word[1]=='S' or word[1]=='A' or word[1]=='V') and word[0] not in months and [sentence, word[2]] not in writtenSentences:
                    try:
                        synod = wn.synsets(word[0], pos = conversion[word[1]])
                    except: # Wordnet ei leidnud sellise sõnavormiga sõna ja viskas erindi, jätame selle sõna vahele
                        continue
                    if len(synod) == 1: # Homonüümsete sõnade vältimiseks jätkame tööd ainult sõnadega, millele leiti üks tähendus
                        question = set() # Sarnased sõnad
                        synonyms = set()
                        wordName = synod[0].name.split('.')[0] # Võtame sõna lemma ehk algvormi
                        # Kontrollime, kas praegune sõna on meie mudeli sõnastikus olemas
                        if wordName not in model.wv.vocab:
                            continue
                        # Leiame sõnale sünonüümid, kui neid on, ning võtame vähemalt 2-tähe pikkused sõnad, et vältida pühapäev -> P sünonüüme
                        for syn in wn.synsets(wordName):
                            for l in syn.lemmas():
                                if l.name != wordName and l.name != word[0] and len(l.name) > 1:
                                    synonyms.add(l.name)
                        # Juhul, kui sünonüüme leiti, võime jätkata. Piisab juba ühest
                        if len(synonyms) >= 1:
                            sims = model.wv.most_similar(positive=[wordName]) # Mudeli abiga leiame sarnaseimad sõnad
                            sims.extend(model.wv.most_similar(negative=[wordName]))
                            filtered = [t for t in sims if t[1] > 0.6 and t[0].isalpha() and len(wn.synsets(t[0])) >= 1] # Filtreerime sarnastest välja ainult üle 0.6 skooriga sõnad, mis on Wordnetis olemas
                            # Kui sõnu jäi alles vähemalt 3, saame jätkata
                            if (len(filtered) >= 3):
                                for item in filtered:
                                    txt = estnltk.Text(item[0])
                                    # Kontrollime, kas sõnavorm kattub küsitava sõna omaga ja sarnane sõna pole kuunimi ega sünonüüm
                                    if (txt.postags[0] == word[1] and txt.lemmas[0] not in months and txt.lemmas[0] not in synonyms):
                                        question.add(item[0])
                        # Kui sarnaseid sõnu tuli vähemalt 3, võime testi hakata looma
                        if len(question) >= 3:
                            answer = ""
                            if len(synonyms) == 1: # Kui sünonüüme on 1, siis see on vastus
                                answer = synonyms.pop()
                            else:
                                # Oleme tsüklis nii kaua, kuni kõik sünonüümid esinevad vähemalt kord testides
                                while True:
                                    answer = random.choice(list(synonyms))
                                    if word[0] not in writtenSynonymsPerWord: # Sõnastik jälgimiseks, mis sünonüümid sõna kohta on juba lisatud
                                        writtenSynonymsPerWord[word[0]] = []
                                    # Kõik sünonüümid on juba lisatud, võimegi jätta juba valitud suvalise vastuse
                                    elif len(writtenSynonymsPerWord[word[0]]) == len(synonyms):
                                        break
                                    # See suvaline sünonüüm on juba testides olemas, kuid on võimalik veel leida sünonüümi, mida pole olemas, otsime edasi
                                    elif answer in writtenSynonymsPerWord[word[0]] and len(writtenSynonymsPerWord[word[0]]) != len(synonyms):
                                        continue
                                    # See sünonüüm on lisamata, jätame meelde, et nüüd on ta võetud
                                    else:
                                        syns = writtenSynonymsPerWord[word[0]]
                                        syns.append(answer)
                                        writtenSynonymsPerWord[word[0]] = syns
                                        break
                            #print("valikud sõna '{s}' lauses {ss} jaoks on: {t}. Sünonüümid on: {k}".format(s=wordName, t=question, k=synonyms, ss=sentence))
                            result = str(idx) + "STP" + untokenize(sentence) + "STP" + word[2]+ "STP" + word[0] + "STP" + ",".join(list(question)) + "STP" + answer
                            f.write(result)
                            f.write("\n")
                            writtenSentences.append([sentence, word[2]])
                            idx += 1
