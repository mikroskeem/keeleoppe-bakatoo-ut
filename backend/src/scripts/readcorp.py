# -*- coding: utf-8 -*-
"""Korpuse sisselugemise skript

See skript loeb sisse TEI-formaadis failid ning eeltöötleb neid.
Eeltöötluse osas eemaldatakse korpusest laused, mis sisaldavad erinevaid soovimatuid märke ning sõnu. Korpusesse kaasatakse ainult 3 kuni 10 sõna pikkused laused.
Lisaks eeltöötlusele luuakse ka Word2Vec mudel, mille abil tehakse teste, ning korpuse lausetest loend.

See skript nõuab, et Gensim ja estNTLK oleksid keskkonda installeeritud.

Seda faili saab moodulina importida ning sisaldab järgmisi funktsioone:
    * readIn - loeb TEI-formaadis faili sisse
    * makeBlackList - loeb sisse nimekirja n-ö mustadest sõnadest ja tagastab neist listi
    * createCorpus - loob korpuse sisseloetavatest failidest, teeb neile eeltöötluse ja treenib nende põhjal mudeli
"""
import re
import estnltk
import os
from estnltk.teicorpus import parse_tei_corpus
import gensim
import logging  # Logimine Gensimi protsesside jälgimiseks
logging.basicConfig(format="%(levelname)s - %(asctime)s: %(message)s", datefmt= '%H:%M:%S', level=logging.INFO)

def readIn(flpath, target, corp):
    """Loeb sisse TEI-formaadis faili

    Parameetrid:
    flpath -- Faili asukoht
    target -- TEI-formaadi tüüpi faili eripärane sihitis
    corp -- Korpus, millele lisatakse juurde

    Tagastab:
    corp -- korpus, mis koosneb töötlemata dokumentidest
    """
    for dirpath, dirnames, files in os.walk(flpath):
        if len(dirnames) > 0 or len(files) == 0:
            continue
        for fl in files:
            flpath = os.path.join(dirpath, fl)
            # Mõne korpuse faili sees TEI-formaadi sisselugemine annab errori, mistõttu on siin try-catch
            try:
                # Kasutame estnltk meetodit TEI-formaadi sisselugemiseks
                docs = parse_tei_corpus(flpath, target=[target], encoding="utf-8")
                for doc in docs:
                    corp.append(doc)
            except:
                continue # Vea leidmisel jätame selle faili vahele
    return corp

def makeBlackList():
    """Loob sobimatute sõnade nimekirja

    Tagastab:
    blacklist -- sõnad, mida me ei taha oma testidesse
    """
    blacklist = []
    with open('src\scripts\must_nimekiri_2020.txt','r', encoding='utf8') as f:
        result = f.read().splitlines()
        blacklist = [x for x in result if x]
    return blacklist

def createCorpus():
    """Loob korpuse ja mudeli. Selleks loetakse sisse failid, tehakse eeltöötlus, valitakse välja piisava pikkusega laused, milles pole keelatud sõnu ega märke.

    Tagastab:
    corpus -- töödeldud laused listina
    model -- treenitud word2vec mudel, mille abil saame teste luua
    """
    documents=[]
    # loeb sisse korpused
    documents = readIn('src\scripts\corpus\Eesti_ajakirjandus', 'artikkel', documents)
    documents = readIn('src\scripts\corpus\Eesti_ilukirjandus\ilukirjandus\Eesti_ilukirjandus_1990', 'tervikteos', documents)
    blacklist = makeBlackList()
    corpus = []
    sentences = []
    punctuation = ['.', '!', '?'] # Lause lõppu lubatud kirjavahemärgid
    for doc in documents:
        tekst = estnltk.Text(doc) # Estnltk abiga saame korpusest kätte laused
        separatedSentences = tekst.sentence_texts
        for sentence in separatedSentences:
            try:
                content = estnltk.Text(sentence).word_texts
                if len(content) > 3 and len(content) < 10:
                    # Eemaldame endline ja tab tühikud
                    sentence = re.sub(r'\n', '', sentence)
                    sentence = re.sub(r'\t', '', sentence)
                    # Asendame valed jutumärgid õigetega
                    sentence = re.sub('”', '"', sentence)
                    sentence = re.sub('“', '"', sentence)
                    sentence = re.sub('”', '"', sentence)
                    sentence = re.sub('“', '"', sentence)
                    # Võtame välja kõik erinevate märkidega laused, et saada lihtsakujulised kätte
                    sentence = re.sub('[<>;\]\[\/\\()_*]', 'INCORRECT', sentence)
                    if 'INCORRECT' in sentence or 'Incorrect' in sentence or sentence.count('"') % 2 != 0 or all(not sentence.endswith(punkt) for punkt in punctuation) or not set(content).isdisjoint(blacklist):
                        continue
                    else:
                        sentences.append(content)
                        corpus.append(sentence)
            except:
                continue

    #model = gensim.models.Word2Vec(min_count=5, size=300, window = 7)  # Tühi mudel, treeningandmeteta
    #model.build_vocab(sentences) # Lisame treeningandmed
    #model.train(sentences, total_examples=model.corpus_count, epochs=50) #Treenime mudeli 50 epohhiga
    #model.init_sims(replace=True)
    #model.save("C:/Users/Janna/Desktop/loputoo/keeleoppe-bakatoo-ut/backend/word2vec.model") # Salvestame mudeli
    # Kui korpused pole muutunud ja mudel on kord juba treenitud, saame kasutada salvestatud varianti
    model = gensim.models.Word2Vec.load('C:/Users/Janna/Desktop/loputoo/keeleoppe-bakatoo-ut/backend/word2vec.model')
    return corpus, model