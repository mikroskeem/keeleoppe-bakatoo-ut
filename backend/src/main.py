# coding=utf-8
"""Keelemängu tagarakenduse põhifail, mida peab jooksutama, et server hakkaks localhost:5000 peal kuulama. Siin on defineeritud kõik ligipääsupunktid.
Selle koodi alguses loetakse sisse testid erineval kujul.

"""
from flask import Flask, jsonify, request, render_template
from entities.entity import Session, engine, Base
from entities.user import User, UserSchema
from entities.test import Test, TestSchema
from entities.user2test import UserTest
from entities.grade import Grades, GradeCategories
from auth import AuthError, requires_auth, requires_role
from flask_cors import CORS, cross_origin
from marshmallow import Schema, INCLUDE, EXCLUDE
from datetime import date
import random
import logging
import os
import sys
sys.path.append("src\scripts\\")
import meaningTest as meaningTest
import grammarTest as grammarTest

app = Flask(__name__)
CORS(app)
app.logger.setLevel(logging.DEBUG)
app.config['JSON_AS_ASCII'] = False

def readIntoDict(tests, type):
    """ Meetod testidest sõnastiku loomiseks, mis on kahel kujul.
    Käändetestide puhul on kujul ID (võti) -> lause, käänatud sõna, sõna algvorm, kääne (väärtus)
    Sünonüümitestide puhul on kujul ID (võti) -> lause, käänatud sõna, sõna algvorm (väärtus)

    Parameetrid:
    tests -- testid, millest sõnastik teha
    type -- testitüüp, kas "meaning" või "grammar"
    Tagastab:
    testsdict -- testist loodud sõnastik
    """
    testsdict = {}
    for test in tests:
        if type == "meaning":
            testsdict[int(test[0])] = [test[1], test[2], test[3]]
        else:
            testsdict[test[0]] = [test[1], test[5], test[3], test[4]]
    return testsdict

def changeGrade(grade, correct):
    """ Meetod kasutaja hinde muutmiseks. Õige vastuse puhul hinnet tõstetakse ühe võrra kõrgemaks, vale puhul ühe võrra madalamaks.
    Parameetrid:
    grade -- Grade objekt, mille hinnet muudetakse
    correct -- kahendmuutuja, kas vastus oli õige
    """
    if correct:
        if grade.grade == GradeCategories.one:
            grade.grade = GradeCategories.two
        else:
            grade.grade = GradeCategories.three
    else:
        if grade.grade == GradeCategories.two:
            grade.grade = GradeCategories.one
        elif grade.grade == GradeCategories.three:
            grade.grade = GradeCategories.two

def readUnsuitableSentences():
    """ Meetod, mis loeb failist kõik sobimatute küsimuste ID-d sisse

    Tagastab:
    unsuitableSents -- loend ebasobilike küsimuste identifikaatoritega
    """
    with open('unsuitableSentences.txt', "r", encoding="utf-8") as f:
        unsuitableSents = f.read().splitlines()
        return unsuitableSents

# Siin loetakse kõik testid sisse ja tehakse neist sõnastikud
unsuitableSents = readUnsuitableSentences()
mTests = meaningTest.readInTests(unsuitableSents)
gTests = grammarTest.readInTests(unsuitableSents)
testsdict = readIntoDict(mTests, 'meaning')
testsdictgrammar = readIntoDict(gTests, 'grammar')

# TODO: hack.
running_in_container = os.getenv("RUNNING_IN_DOCKER", 'false').lower() == 'true'

def initialize_app_data():
    Base.metadata.create_all(engine, checkfirst=True)
    # Lisame testid failidest andmebaasi
    session = Session()
    for test in mTests:
        test = Test(test[0], test[5], 'meaning')
        session.add(test)
        session.commit()
    for test in gTests:
        test = Test(test[0], test[3], 'grammar')
        session.add(test)
        session.commit()
    session.close()

# Rakenduse esmakordsel töölepanekul võtta siit kommentaarid ära, seejärel tagasi panna
# create_all() loob puuduvad tabelid andmebaasi

# initialize_app_data()

### Defineerime punktid, kust saab rakendusele ligi

@app.route('/userdata', methods=['GET'])
def get_data():
    """ Route, kust päritakse profiili infot """
    user_id = request.args.get('user_id')
    if not user_id:
        return {"message": "No input data provided"}, 400
    # Uhendume andmebaasiga
    session = Session()
    # Pärime andmebaasist erinevad protsendid ja sõnad, mille puhul enim on eksitud
    overall_percentage_query = session.execute("SELECT correct, COUNT(*) * 100.0 / SUM(COUNT(*)) OVER() AS result FROM usertest WHERE user_id = :val GROUP BY correct", {'val': user_id})
    grammar_percentage_query = session.execute("SELECT usertest.correct, COUNT(*) * 100.0 / SUM(COUNT(*)) OVER() AS result FROM usertest INNER JOIN tests ON usertest.question_id = tests.question_id WHERE tests.test_type='grammar' AND user_id = :val GROUP BY usertest.correct",  {'val': user_id})
    meaning_percentage_query = session.execute("SELECT usertest.correct, COUNT(*) * 100.0 / SUM(COUNT(*)) OVER() AS result FROM usertest INNER JOIN tests ON usertest.question_id = tests.question_id WHERE tests.test_type='meaning' AND user_id = :val GROUP BY usertest.correct",  {'val': user_id})
    mostwrong_meaning_query = session.execute("SELECT tests.correct_answer, COUNT(*) AS answerCount, GROUP_CONCAT(CONCAT(usertest.question_id, ' ',usertest.given_answer)) as answers FROM usertest INNER JOIN tests ON usertest.question_id = tests.question_id WHERE usertest.correct = False AND tests.test_type= 'meaning' AND usertest.user_id= :val GROUP BY  tests.correct_answer ORDER BY COUNT(usertest.given_answer) DESC LIMIT 3", {'val': user_id})
    mostwrong_grammar_query = session.execute("SELECT tests.correct_answer, COUNT(*) AS answerCount, GROUP_CONCAT(CONCAT(usertest.question_id, ' ',usertest.given_answer)) as answers FROM usertest INNER JOIN tests ON usertest.question_id = tests.question_id WHERE usertest.correct = False AND tests.test_type= 'grammar' AND usertest.user_id= :val GROUP BY  tests.correct_answer ORDER BY COUNT(usertest.given_answer) DESC LIMIT 3", {'val': user_id})
    session.close()
    overall_percentage = 0
    grammar_percentage = 0
    meaning_percentage = 0
    # Võtame kõik päringud eraldi lahti
    for r in meaning_percentage_query:
        if r[0] == False:
            meaning_percentage = int(r['result'])
    for r in grammar_percentage_query:
        if r[0] == False:
            grammar_percentage = int(r['result'])
    for r in overall_percentage_query:
        if r[0] == False:
            overall_percentage = int(r['result'])
    # Päringu tüüp ei ole tavaliselt ligipääsetav, mistõttu peame tulemuse uuesti listi lugema
    mostWrong = [r for r in mostwrong_meaning_query]
    mostWrongGrammar = [r for r in mostwrong_grammar_query]
    mostWrong = [list(row) for row in mostWrong]
    mostWrongGrammar = [list(row) for row in mostWrongGrammar]
    # Ühes reas on õige sõna, mitu korda tema vastu on eksitud, näitelause
    for row in mostWrong:
        # Võtame rea osadeks, praegu on ta ühe lausena
        row[2] = [item.split(" ") for item in row[2]]
        for idx, word in row[2]:
            row[2] = [testsdict[int(idx)],word]
    for row in mostWrongGrammar:
        row[2] = [item.split(" ") for item in row[2]]
        for idx, word in row[2]:
            row[2] = [testsdictgrammar[int(idx)],word]
    return jsonify([mostWrong, overall_percentage, grammar_percentage, meaning_percentage, mostWrongGrammar])

@app.route('/tests/<type>/<sentenceNr>',  methods=['GET'])
def get_test_by_type(type, sentenceNr):
    """ Route, kust päritakse teste """
    user_id = request.args.get('user_id')
    if not user_id:
        return {"message": "No input data provided"}, 400
    session = Session()
    testsDone = []
    # Vastavalt tüübile päritakse hindeid
    if type == 'm':
        testsDone = session.execute("SELECT grade, question, done_question_ids FROM grades WHERE user_id = :var AND word_case = ''", {"var":user_id})
    elif type == 'g':
        testsDone = session.execute("SELECT grade, question, done_question_ids, word_case FROM grades WHERE user_id = :var AND word_case != ''", {"var":user_id})
    testsDone = [r for r in testsDone]
    # Segame läbi, et mitte saada samas järjekorras vigaseid lauseid iga kord, kindlustame minimaalse varieeruvuse
    random.shuffle(testsDone)
    session.close()
    tests = []
    #print("Otsime teste kasutaja {} jaoks".format(user_id))
    # Otsime teste
    if type == 'm':
        tests = meaningTest.chooseTests(int(sentenceNr), mTests, testsDone)
    elif type == 'g':
        tests = grammarTest.chooseTests(int(sentenceNr), gTests, testsDone)
    return jsonify(tests)

@app.route('/submit', methods=['POST'], endpoint="save_test_results")
@cross_origin(origin='localhost',headers=['Content-Type','Authorization'])
def save_test_results():
    """ Salvestame kasutaja vastused andmebaasi """
    ttype = request.args.get('test_type')
    if not ttype:
        return {"message": "No test type provided"}, 400
    resp = request.get_json()
    session = Session()
    # Vaatame ükshaaval vastatud teste
    for item in resp:
        user2test = UserTest(item['given_answer'], item['correct'], item['user_id'], item['question_id'])
        session.add(user2test) # Lisame testi
        # Uuendame hindeid
        question_id = int(item['question_id'])
        grade = None
        if ttype == 'm':
            grade = session.query(Grades).filter_by(user_id=item['user_id'], question=testsdict[question_id][2]).first()
        elif ttype == 'g':
            grade = session.query(Grades).filter_by(user_id=item['user_id'], question=testsdictgrammar[question_id][3]).first()
        if grade is not None:
            # Uuendame olemasolevat hinnet
            #print("uuendame olemasolevat hinnet kasutajal {}: ".format(item['user_id']))
            grade.add_question_id(question_id)
            changeGrade(grade, item['correct'])
        else:
            # Hinnet veel pole, lisame selle sõnaga hinde
            #print("lisame uue hinde kasutajale {}".format(item['user_id']))
            if item['correct']:
                if ttype == 'm':
                    grade = Grades(GradeCategories.two,testsdict[question_id][2], item['user_id'], question_id)
                elif ttype == 'g':
                    grade = Grades(GradeCategories.two,testsdictgrammar[question_id][3].lower(), item['user_id'], question_id, testsdictgrammar[question_id][2])
            else:
                if ttype == 'm':
                    grade = Grades(GradeCategories.one,testsdict[question_id][2], item['user_id'], question_id)
                elif ttype == 'g':
                    grade = Grades(GradeCategories.one,testsdictgrammar[question_id][3].lower(), item['user_id'], question_id, testsdictgrammar[question_id][2])
            session.add(grade)
        session.commit()
    session.close()
    return jsonify("Testid lisatud andmebaasi"), 201

@app.route('/users', methods=['POST', 'GET'], endpoint="add_user")
@cross_origin(origin='localhost',headers=['Content-Type'])
def add_user():
    """ Kasutaja lisamise punkt, mida kutsutakse välja igal registreerimisel ja sisselogimisel """
    if not request.get_json():
        return {"message": "No input data provided"}, 400
    # Hangime requestist loodava kasutaja info ja loome kasutaja objekti
    user = UserSchema(only=('user_id', 'name')).load(request.get_json(), unknown=EXCLUDE)
    session = Session()
    # Kontrollime, kas kasutaja on andmebaasis olemas, kui ei, lisame ta sinna, et tema Auth0 identifikaator meelde jätta
    exist = session.query(User).filter_by(user_id=user.user_id).all()
    if len(exist)==0:
        session.add(user)
        session.commit()
        schema = UserSchema()
        new_user = schema.dump(user)
        session.close()
        return jsonify(new_user), 201
    elif len(exist) > 0:
        session.close()
        return "Kasutaja on juba andmebaasis", 200
    else:
        session.close()
        return "Midagi läks valesti", 500

@app.route('/tests/report', methods=['POST'], endpoint='report_test')
@cross_origin(origin='localhost',headers=['Content-Type','Authorization'])
def report_test():
    """ Route testist teavitamiseks """
    if not request.get_json():
        return {"message": "No input data provided"}, 400
    id = request.get_json()['test_id']
    ttype = request.get_json()['test_type']
    session = Session()
    # Kui test ei sobi, peame ta eemaldama tehtud testide ja hinnete seast, sest muidu võib valeinfo sattuda profiili ja järgmistesse testidesse
    user2tests = session.query(Test).filter_by(question_id=id).all()
    grades = session.query(Grades).filter(Grades.done_question_ids.any(id)).all()
    if len(user2tests) > 0:
        #print("kustutame user2testi reporti tõttu")
        session.execute("DELETE FROM usertest WHERE question_id = :var", {"var": id})
    if len(grades) > 0:
        #print("eemaldame testi hinnetest reporti tõttu")
        for grade in grades:
            grade.delete_question_id(id)
    session.commit()
    session.close()
    # Loeme testid uuesti sisse, et valet testi mitte näidata rohkem
    with open('unsuitableSentences.txt', "a") as f:
        f.write(str(id))
        f.write("\n")
    global unsuitableSents
    unsuitableSents = readUnsuitableSentences()
    if ttype == 'meaning':
        global mTests
        global testsdict
        mTests = meaningTest.readInTests(unsuitableSents)
        testsdict = readIntoDict(mTests, 'meaning')
    else:
        global gTests
        global testsdictgrammar
        gTests = grammarTest.readInTests(unsuitableSents)
        testsdictgrammar = readIntoDict(gTests, 'grammar')
    return 'Sobimatust lausest teavitamine läks edukalt!', 200


@app.errorhandler(AuthError)
def handle_auth_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response

if __name__== "__main__":
    if running_in_container:
        if not os.path.exists('/data/init_done.txt'):
            try:
                initialize_app_data()

                with open('/data/init_done.txt', 'w') as f:
                    f.write('yep')
            except Exception as e:
                print("failed to initialize app with initial data")
                raise e

    app.run(debug=True)
